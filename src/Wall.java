import java.awt.*;
import java.nio.FloatBuffer;
import java.util.*;
import javax.media.opengl.*;
import com.sun.opengl.util.*;
import com.sun.opengl.util.texture.*;
/**
 * This class represents a wall.
 * 
 * Modified to render in 3D, calculate collisions,
 * and added a render3DFlexible which adds the option
 * of Portal/door style rendering. Allows portal and door
 * to re-use code from this class.
 * 
 * @author Tim Lambert
 * @author Luke Cameron
 *
 */
public class Wall extends FPPolygon {

	public String extraName(int i) {
		return i == 0 ? "Base" : "Top";
	}

	public float lineWidth() {
		return 4.0f;
	}
	
	// new method
	public Vector3D collide(Vector3D from, Vector3D to) {
		
		// rule out y-axis first
		double bottom = extra[0];
		double top = extra[1];
		double avatarBottom = from.y - Avatar.height;
		double avatarTop = 42;
		if (avatarBottom > top || avatarTop < bottom) {
			return null;
		}
		
		if (contains((int)Math.round(to.x), (int)Math.round(to.z))) {
			return from;
		}
		return null;
	}

	/** paint the wall using gl. */
	public void paint(GL gl, GLDrawable glc) {

		if (fill != null) {
			setColor(gl, fill);
			gl.glDisable(GL.GL_TEXTURE_2D);
		}
		gl.glLineWidth(lineWidth());
		gl.glBegin(GL.GL_LINES); // draw the wall
		for (int i = 0; i < 2; i++) {
			Point2D p = (Point2D) (pts2d.get(i));
			gl.glVertex2d(p.x, p.y);
		}
		gl.glEnd();
	}

	// changed method
	public void render3D(GL gl, GLDrawable glc) {
		render3DFlexible(gl, glc, false);
	}
	
	// new method
	public void render3DFlexible(GL gl, GLDrawable glc, boolean useSquish) {
		// don't draw unless there are 2 points
		if (pts2d.size() != 2) {
			return;
		}

		if (fill != null || texture != null) {
			// get the points required
			Point2D p0 = pts2d.get(0);
			Point2D p1 = pts2d.get(1);
			Vector3D p0l = new Vector3D(p0.x, extra[0], p0.y);
			Vector3D p0h = new Vector3D(p0.x, extra[1], p0.y);
			Vector3D p1l = new Vector3D(p1.x, extra[0], p1.y);
			Vector3D p1h = new Vector3D(p1.x, extra[1], p1.y);
			Vector3D[] quad = { p0l, p0h, p1h, p1l };

			// draw the object
			if (texture == null) {
				setLighting(gl);
				gl.glDisable(GL.GL_TEXTURE_2D);
				Utils.drawRectangle(gl, quad);
			} else {
				setLightingTexture(gl);
				Texture gltexture = texture.getTexture(glc);
				if (useSquish)
					Utils.drawRectangleTexturedSquish(gl, quad, gltexture, scale);
				else
					Utils.drawRectangleTexturedCopy(gl, quad, gltexture, scale);
			}
		}
	}

	/** add a control point */
	public void addPoint(Point p) {
		if (pts2d.size() < 2) {
			super.addPoint(p);
		}
	}

	/** remove specified control point */
	public void removePoint() {
		return;
	}

	public boolean contains(int x, int y) {
		Edge2D e = new Edge2D((Point2D) pts2d.get(0), (Point2D) pts2d.get(1));
		Point2D pe = e.toLineSpace(new Point(x, y));
		return (pe.getX() > 0 && pe.getX() < 1 && Math.abs(pe.getY()) < EPSILON);
	}

}
