/**
 * An interface that represents an object
 * that has a constant acceleration associated
 * with it (e.g. gravity)
 * 
 * 
 * @author Luke Cameron
 * 
 */

public interface IForcer {
	public Vector3D acc();
}
