import java.awt.*;
import java.util.*;
import javax.media.opengl.*;
import com.sun.opengl.util.*;
import com.sun.opengl.util.texture.*;

/** 
 * 
 * This class represents a portal.
 * 
 * Modified to render in 3D and teleport the avatar on collision.
 * 
 * @author Tim Lambert
 * @author Luke Cameron
 * 
 */

public class Portal extends FPPolygon {

	public final double portalHeight = 100.0;

	public String extraName(int i) {
		return i == 0 ? "Start Height" : "End Height";
	}

	public float lineWidth() {
		return 4.0f;
	}
	
	// new method
	public Vector3D collide(Vector3D from, Vector3D to) {
		if (pts2d.size() != 4) {
			return null;
		}
		
		// rule out y-axis first
		double bottom1 = extra[0];
		double top1 = extra[0] + portalHeight;
		double bottom2 = extra[1];
		double top2 = extra[1] + portalHeight;
		double avatarBottom = from.y - Avatar.height;
		double avatarTop = from.y;
		if (avatarBottom > top1 || avatarTop < bottom1) {
			return null;
		}
		
		Vector3D portal1 = Vector3D.fromPointAlternative(pts2d.get(0)).interp(Vector3D.fromPointAlternative(pts2d.get(1)), 0.5);
		portal1.y = bottom1;
		
		Vector3D portal2 = Vector3D.fromPointAlternative(pts2d.get(2)).interp(Vector3D.fromPointAlternative(pts2d.get(3)), 0.5);
		portal2.y = bottom2;
		
		Vector3D directionBetween = portal2.subtract(portal1).normalize();
		
		// if entering portal 1, transport to portal 2
		if (contains(0, (int)Math.round(to.x), (int)Math.round(to.z))) {
			return portal2.add(new Vector3D(0, Avatar.height, 0));
		}
		
		return null;
	}

	/** paint the portal using gl. */
	public void paint(GL gl, GLDrawable glc) {

		if (fill != null) {
			setColor(gl, fill);
			gl.glDisable(GL.GL_TEXTURE_2D);
		}
		gl.glLineWidth(lineWidth());
		gl.glBegin(GL.GL_LINES); // draw each wall
		for (int i = 0; i < pts2d.size(); i++) {
			Point2D p = (Point2D) (pts2d.get(i));
			gl.glVertex2d(p.x, p.y);
		}
		gl.glEnd();

		if (pts2d.size() == 4) {
			/* Connect mid point of the two line segments with dotted line */
			gl.glLineStipple(1, (short) 0x0F0F);
			gl.glEnable(GL.GL_LINE_STIPPLE);
			gl.glLineWidth(1);
			gl.glBegin(GL.GL_LINES);
			Point2D mid = pts2d.get(0).interp(pts2d.get(1), 0.5);
			gl.glVertex2d(mid.x, mid.y);
			mid = pts2d.get(2).interp(pts2d.get(3), 0.5);
			gl.glVertex2d(mid.x, mid.y);
			gl.glEnd();
			gl.glDisable(GL.GL_LINE_STIPPLE);
		}
	}

	// changed method
	public void render3D(GL gl, GLDrawable glc) {
		if (pts2d.size() >= 4 && (fill != null || texture != null)) {
			// create some points
			Vector3D[] pointsEntrance = {
					new Vector3D(pts2d.get(0).x, extra[0], pts2d.get(0).y),
					new Vector3D(pts2d.get(0).x, extra[0] + portalHeight, pts2d.get(0).y),
					new Vector3D(pts2d.get(1).x, extra[0] + portalHeight, pts2d.get(1).y),
					new Vector3D(pts2d.get(1).x, extra[0], pts2d.get(1).y)
			};
			Vector3D[] pointsExit = {
					new Vector3D(pts2d.get(2).x, extra[1], pts2d.get(2).y),
					new Vector3D(pts2d.get(2).x, extra[1] + portalHeight, pts2d.get(2).y),
					new Vector3D(pts2d.get(3).x, extra[1] + portalHeight, pts2d.get(3).y),
					new Vector3D(pts2d.get(3).x, extra[1], pts2d.get(3).y)
			};
			// draw the object
			if (texture == null) {
				setLighting(gl);
				gl.glDisable(GL.GL_TEXTURE_2D);
				Utils.drawRectangle(gl, pointsEntrance);
				Utils.drawRectangle(gl, pointsExit);
			} else {
				setLightingTexture(gl);
				Texture gltexture = texture.getTexture(glc);
				Utils.drawRectangleTexturedSquish(gl, pointsEntrance, gltexture, scale);
				Utils.drawRectangleTexturedSquish(gl, pointsExit, gltexture, scale);
			}
		}
	}

	/** add a control point */
	public void addPoint(Point p) {
		if (pts2d.size() < 4) {
			super.addPoint(p);
		}
	}

	/** remove specified control point - not allowed for a portal */
	public void removePoint() {
		return;
	}

	// utility for contains takes an offset so we can check the two parts of the
	// portal
	public boolean contains(int offset, int x, int y) {
		Edge2D e = new Edge2D(pts2d.get(offset), pts2d.get(offset + 1));
		Point2D pe = e.toLineSpace(new Point(x, y));
		return (pe.getX() > 0 && pe.getX() < 1 && Math.abs(pe.getY()) < EPSILON);
	}

	public boolean contains(int x, int y) {
		return contains(0, x, y) || contains(2, x, y);
	}

}
