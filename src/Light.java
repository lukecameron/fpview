import java.awt.*;
import java.util.*;
import javax.media.opengl.*;

/**
 * This class represents a light
 * 
 * Modified to animate state changes.
 * 
 * @author Tim Lambert
 * @author Luke Cameron
 */

public class Light extends FPPolygon {

	private double radius = 6; // radius of circle
	final int framesAnimate = 15;
	private int framesLeft = 0;

	boolean isOn = true;

	// only report being off is state is off AND no animation frames are left
	// changed method
	public boolean isOn() {
		return isOn || framesLeft > 0 ? true : false;
	}
	
	// new method
	public void advance() {
		if (framesLeft > 0) {
			framesLeft--;
		}
	}
	
	// new method
	double currentFade() {
		double fa = (double) framesAnimate;
		double fl = (double) framesLeft;
		return isOn ? (fa - fl) / fa : fl / fa;
	}
	
	// new method
	public Vector3D collide(Vector3D from, Vector3D to) {
		return null;
	}

	public Color getFillColor() {
		double red = fill.getRed() * currentFade();
		double green = fill.getGreen() * currentFade();
		double blue = fill.getBlue() * currentFade();

		return new Color((int) red, (int) green, (int) blue);
	}
	
	// changed method
	public void toggle() {
		isOn = !isOn;
		framesLeft = framesAnimate - framesLeft;
	}

	public float[] getPosition() {
		float[] pos = new float[4];
		Point2D p0 = pts2d.get(0);
		pos[0] = (float) p0.x;
		pos[2] = (float) p0.y;
		pos[1] = (float) extra[0];
		pos[3] = 1;
		return pos;
	}

	public float lineWidth() {
		return 2.0f;
	}

	/** add a control point */
	public void addPoint(Point p) {
		if (pts2d.size() < 1) {
			super.addPoint(p);
		}
	}

	/** paint this light into g. */
	public void paint(GL gl, GLDrawable glc) {
		if (fill != null) {
			setColor(gl, fill);
			gl.glDisable(GL.GL_TEXTURE_2D);
		}
		gl.glLineWidth(lineWidth());
		gl.glBegin(GL.GL_POLYGON); // draw the light

		Point2D centre = (Point2D) (pts2d.get(0));

		for (int i = 0; i < 40; i++) {
			double angle = (i / 40.0) * 2 * Math.PI;
			gl.glVertex2d(centre.x + radius * Math.cos(angle), centre.y
					+ radius * Math.sin(angle));
		}
		gl.glEnd();
	}

	public boolean contains(int x, int y) {
		Point2D centre = (Point2D) (pts2d.get(0));
		double dx = x - centre.x;
		double dy = y - centre.y;
		return (Math.sqrt(dx * dx + dy * dy) < radius);
	}

}
