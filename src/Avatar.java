import java.awt.*;
import java.util.*;
import javax.media.opengl.*;
import com.sun.opengl.util.*;

/**
 * 
 * Represents an avatar.
 * 
 * Modified to have a method for turning.
 * 
 * @author Tim Lambert
 * @author Luke Cameron
 * 
 */
public class Avatar implements IBoidObject {
	public static final double height = 80; // avatar's height in world coords
	final double moveStep = 4; 				// distance in world coords to move per step
	final double turnStep = 0.16; 			// angle to rotate per step
	double turnIncrement; 					// signed version of turnStep. Gets updated when turn is called
	double moveIncrement; 					// signed version of moveStep. Gets updated when move is called
	int turnframes; 						// number of frames left to turn in
	int moveframes; 						// number of frames left to move in
	Vector3D pos; 							// current position
	Vector3D dir; 							// current direction

	float t = 0;

	public Avatar(Vector3D pos, Vector3D dir) {
		this.pos = pos;
		this.dir = dir;
	}

	// move forward or back for given number of frames
	public void move(int noframes) {
		if (noframes > 0) {
			moveIncrement = moveStep;
		} else {
			moveIncrement = -moveStep;
		}
		moveframes = Math.abs(noframes);
	}

	// rotate left or right for a given number of frames
	// new method
	public void turn(int noframes) {
		if (noframes > 0) {
			turnIncrement = turnStep;
		} else {
			turnIncrement = -turnStep;
		}
		turnframes = Math.abs(noframes);
	}

	// update position and direction, checking for collisions
	// changed method
	public void advance(FPShapeList shapeList) {

		// update dir
		if (turnframes > 0) {
			dir = dir.rotatey(turnIncrement);
			turnframes--;
		}
		
		// update pos
		Vector3D newpos;
		if (moveframes > 0) {
			newpos = pos.add(dir.scale(moveIncrement));
			moveframes--;
		} else {
			newpos = pos; // not moving
		}
		pos = shapeList.collide(pos, newpos);
	}

	public Vector3D currentPos() {
		return pos;
	}

	public Vector3D currentDir() {
		return dir;
	}

	public void setColor(GL gl, Color c) {
		gl.glColor3ub((byte) c.getRed(), (byte) c.getGreen(),
				(byte) c.getBlue());
	}
	
	// changed method
	public void render3D(GL gl, GLDrawable glc) {
		setColor(gl, Color.white);
		gl.glDisable(GL.GL_TEXTURE_2D);

		gl.glPushMatrix();
		gl.glTranslated(pos.x, pos.y - height / 2, pos.z);
		gl.glRotated(-Math.toDegrees(dir.glAngle()), 0, 1, 0);
		gl.glScaled(5, height, 25);
		
		GLUT glut = new GLUT();
		glut.glutSolidCube(1);
		gl.glPopMatrix();
	}

	// new method
	@Override
	public Vector3D pos() {
		return pos;
	}
	
	
	// new method
	@Override
	public Vector3D collide(Vector3D from, Vector3D to) {
		return null;
	}

}
