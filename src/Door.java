import java.awt.*;
import java.util.*;
import javax.media.opengl.*;
import com.sun.opengl.util.*;
import com.sun.opengl.util.texture.*;
/**
 * 
 * This class represents a door.
 * 
 * Modified to have an advance function to advance the animation between states,
 * and a toggleState function to open or close the door.
 * 
 * @author Tim Lambert
 * @author Luke Cameron
 * 
 */

public class Door extends Wall {
	
	final int ANIM_FRAMES = 100;
	final double STEP_ANGLE = Math.PI / 2 / ANIM_FRAMES;
	
	int framesLeft; // number of frames remaining of motion
	boolean isOpen = false; // whether door is open or closed
	
	
	
	public float lineWidth() {
		return 2.0f;
	}
	
	// new method
	public void toggleState() {
		if (isOpen) {
			System.err.println("closing door!");
		} else {
			System.err.println("opening door!");
		}
		isOpen = !isOpen;
		
		// subtract framesLeft to prevent door from rotating
		// off its arc
		framesLeft = ANIM_FRAMES - framesLeft;
	}
	
	// new method
	public void advance() {
		if (framesLeft > 0 && pts2d.size() >= 2) {
			Vector3D p0 = new Vector3D(pts2d.get(0));
			Vector3D p1 = new Vector3D(pts2d.get(1));
			if (!isOpen) {
				p1 = p1.subtract(p0).rotatez(-STEP_ANGLE).add(p0);
			} else {
				p1 = p1.subtract(p0).rotatez(STEP_ANGLE).add(p0);
			}
			
			pts2d.set(1, new Point2D(p1.x, p1.y));
			framesLeft--;
		}
	}
	
	/** paint this door into g. */
	public void paint(GL gl, GLDrawable glc) {

		if (fill != null) {
			setColor(gl, fill);
			gl.glDisable(GL.GL_TEXTURE_2D);
		}
		gl.glLineWidth(lineWidth());
		gl.glBegin(GL.GL_LINE_STRIP); // draw the wall

		Point2D centre = (Point2D) (pts2d.get(0));
		Point2D rad = (Point2D) (pts2d.get(1));
		double dx = rad.x - centre.x;
		double dy = rad.y - centre.y;
		double radius = Math.sqrt(dx * dx + dy * dy);
		double theta = Math.atan2(dy, dx);

		gl.glVertex2d(centre.x, centre.y);
		for (int i = 0; i < 10; i++) {
			double angle = theta + (i / 10.0) * Math.PI / 2.0;
			gl.glVertex2d(centre.x + radius * Math.cos(angle), centre.y
					+ radius * Math.sin(angle));
		}
		gl.glEnd();
	}
	
	// changed method
	public void render3D(GL gl, GLDrawable glc) {
		render3DFlexible(gl, glc, true);
	}
}
