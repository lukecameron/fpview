import java.awt.*;
import java.util.*;
import javax.media.opengl.*;
import com.sun.opengl.util.*;
import com.sun.opengl.util.texture.*;
/**
 * This class represents a window. 
 *
 * Modified to render in 3D. 
 *
 */
public class Window extends Wall {

	public float lineWidth() {
		return 2.0f;
	}

	// changed method
	public void render3D(GL gl, GLDrawable glc) {
		if (pts2d.size() != 2) {
			return;
		}

		if (fill != null || texture != null) {

			// work out some parameters
			Vector3D p0 = new Vector3D(pts2d.get(0));
			Vector3D p1 = new Vector3D(pts2d.get(1));
			double bottom = extra[0];
			double top = extra[1];
			double oneThirdUp = (top - bottom) / 3 + bottom;
			double twoThirdsUp = oneThirdUp + (top - bottom) / 3;
			Vector3D[] topPoints = { new Vector3D(p0.x, bottom, p0.y),
					new Vector3D(p0.x, oneThirdUp, p0.y),
					new Vector3D(p1.x, oneThirdUp, p1.y),
					new Vector3D(p1.x, bottom, p1.y) };
			Vector3D[] bottomPoints = { new Vector3D(p0.x, twoThirdsUp, p0.y),
					new Vector3D(p0.x, top, p0.y),
					new Vector3D(p1.x, top, p1.y),
					new Vector3D(p1.x, twoThirdsUp, p1.y) };

			if (texture == null) {
				// draw without texture
				setLighting(gl);
				gl.glDisable(GL.GL_TEXTURE_2D);
				Utils.drawRectangle(gl, topPoints);
				Utils.drawRectangle(gl, bottomPoints);

			} else {
				// draw with texture
				setLightingTexture(gl);
				Utils.drawRectangleTexturedCopy(gl, topPoints,
						texture.getTexture(glc), scale);
				Utils.drawRectangleTexturedCopy(gl, bottomPoints,
						texture.getTexture(glc), scale);
			}
		}

	}
}
