/**
 * An interface that represents an object in the
 * Boids world.
 * 
 * 
 * @author Luke Cameron
 * 
 */

public interface IBoidObject {
	public Vector3D pos();
	public Vector3D collide(Vector3D from, Vector3D to);
}
