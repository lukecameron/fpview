import java.nio.FloatBuffer;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import javax.media.opengl.*;
import javax.media.opengl.glu.*;

import com.sun.opengl.util.*;

/**
 * Program to display floorplans designed using FPED
 * 
 * The code is copyright (C) Waleed Kadous 2001 and copyright (C) Tim Lambert
 * 2010
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * Almost all of the methods are modified, so they are not marked as such.
 * 
 * Keyboard state tracker inspired by
 * http://www.gamedev.net/page/resources/_/technical
 * /general-programming/java-games-keyboard-and-mouse-r2439
 * 
 * Idea for Live Editing and sharing/injecting of shape list came from Damon
 * Stacey. No code was exchanged, only the idea.
 * 
 * 
 * @author Waleed Kadous
 * @author Tim Lambert
 * @author Luke Cameron
 * 
 */

public class FPView extends Frame implements GLEventListener, KeyListener,
		MouseListener, MouseMotionListener {

	FPShapeList shapeList = null; // List of shapes that we have to render

	int appHeight = 400; // Height of the browsing window
	int appWidth = 400; // Width of the browsing window

	int prevx = 0; // Previously observed x value of mouse click.
	int prevy = 0; // Previously observed y value of mouse click.

	GLCanvas glc = null;

	boolean isDay = false;

	Vector3D viewpos = new Vector3D(10, 60, 30); // Default viewing position
	Vector3D viewdir = new Vector3D(1, 0, 1); // Default viewing direction
												// (diagonal across world)
	
	int frameCounter;
	Bird testBird;
	
	Avatar avatar;

	float clearColorRed = 0.1f;
	float clearColorGreen = 0.1f;
	float clearColorBlue = 0.1f;

	// store the states of the keys so that we can smoothly move the avatar when
	// a key is held down
	private static final int NUM_KEYS = 256;
	private KeyState[] keyStates;

	// store the index of the current viewport, or -1 if there one hasn't been
	// set
	private int currentViewPortIndex = -1;

	static final int FIRST_PERSON_VIEW = 1;
	static final int THIRD_PERSON_VIEW = 2;
	int viewType = THIRD_PERSON_VIEW; // Is view from avatar, or is it 3rd
										// person view?

	static final double ROTATE_FACTOR = 360;
	
	BoidManager boidManager = new BoidManager();
	List<Bird> birds = new ArrayList<Bird>();

	Point prevMousePos = null;

	public FPView(String filename, boolean attachEditor) {
		super("Floor Plan Viewer: " + filename); // Create window with title

		// initialise the keyStates array
		keyStates = new KeyState[NUM_KEYS];
		for (int i = 0; i < NUM_KEYS; i++) {
			keyStates[i] = KeyState.UP;
		}
		
		// set framecounter to zero
		frameCounter = 0;

		shapeList = new FPShapeList();
		try {
			shapeList.read(filename);
		} catch (Exception e) {
			System.err.println("Could not load the floor plan " + filename);
			e.printStackTrace();
			System.exit(1);
		}

		// create the editor window with injected shapelist
		// idea for sharing the ShapeList came from Damon Stacey
		if (attachEditor) {
			FPEdit.makeWithInjectedShapeList(shapeList);
		}

		setLayout(new BorderLayout());

		// create a GL canvas with an eight-bit stencil buffer
		GLCapabilities c = new GLCapabilities();
		c.setStencilBits(8);
		glc = new GLCanvas(c);

		// quit if the window closes
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// More initialisation.
		glc.addGLEventListener(this);
		glc.addKeyListener(this);
		glc.addMouseListener(this);
		glc.addMouseMotionListener(this);

		add("Center", glc);
		pack();
		setSize(appHeight, appWidth);

		if (this.shapeList.getViews().size() > 0) {
			cycleToNextViewPort();
		}

		// Run animation in a tight loop.
		Animator animator = new FPSAnimator(glc, 30); // animate at 30 fps

		animator.start();

		setVisible(true);

	}

	// handles args and creates an FPView accordingly.
	// handles experimental mode
	public static void main(String[] args) {

		if (args.length > 1) {
			if (args[1].equals("--edit")) {
				FPView dsview = new FPView(args[0], true);
			}
		}

		else if (args.length == 1) {
			FPView dsview = new FPView(args[0], false);
		}
	}

	/** Methods that must be implemented for Interface fulfillment */

	/** For GLEventListener */
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

		avatar = new Avatar(new Vector3D(0, Avatar.height, 75), new Vector3D(1,
				0, 0));
		boidManager = new BoidManager();
		boidManager.addAttractor(avatar);
		//testBird = new Bird();
		//testBird.setPos(new Vector3D(-10, 0, -10));
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(60, 1, 1, 10000);

		gl.glEnable(GL.GL_DEPTH_TEST);

		appWidth = width;
		appHeight = height;

	}

	// new method
	private void lightScene(GL gl) {
		// traverse through lights in scene
		List<Light> li = shapeList.getLights();
		int count = GL.GL_LIGHT1;
		for (Light l : li) {
			if (count <= GL.GL_LIGHT7) {
				Color c = l.getFillColor();
				float[] diffCol = c.getColorComponents(null);
				float[] specCol = { 0, 0, 0, 0 };
				if (l.isOn()) {
					gl.glEnable(count);
				} else {
					gl.glDisable(count);
				}
				gl.glLightfv(count, GL.GL_POSITION,
						FloatBuffer.wrap(l.getPosition()));
				gl.glLightfv(count, GL.GL_DIFFUSE, FloatBuffer.wrap(diffCol));
				gl.glLightfv(count, GL.GL_SPECULAR, FloatBuffer.wrap(specCol));

				// set attenuation values
				gl.glLightf(count, GL.GL_CONSTANT_ATTENUATION, 1.0f);
				gl.glLightf(count, GL.GL_LINEAR_ATTENUATION, 0f);
				gl.glLightf(count, GL.GL_QUADRATIC_ATTENUATION, 0.0f);
			}
			count++;
		}

		// add the sun
		if (isDay) {
			gl.glEnable(GL.GL_LIGHT0);
			float[] sundir = { -1, 1, -1, 0 };
			float[] diffCol = { 0.6f, 0.6f, 0.6f, 1 };
			float[] specCol = { 0, 0, 0, 0 };

			gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, FloatBuffer.wrap(sundir));
			gl.glLightfv(count, GL.GL_DIFFUSE, FloatBuffer.wrap(diffCol));
			gl.glLightfv(count, GL.GL_SPECULAR, FloatBuffer.wrap(specCol));

		} else {
			gl.glDisable(GL.GL_LIGHT0);
		}
	}

	// new method
	public void toggleLight(int lightNo) {
		List<Light> li = shapeList.getLights();
		if (li.size() > lightNo) {
			li.get(lightNo).toggle();
		} else {
			System.err.printf("tried to toggle light %d, not enough lights!\n",
					lightNo);
		}
	}

	// draw the scene.
	public void display(GLAutoDrawable drawable) {
		
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		
		// update animatables
		shapeList.advanceAnimation();
		updateAvatar();
		updateBirds();

		// not doing portal look-through due to wanting to do other things
		//portalRender(gl, glu);

		// run the set-up routine.
		setUpScene(gl, glu);

		// Render the shapes.
		//gl.glPushMatrix();
		//gl.gl
		//testBird.update();
		//testBird.render3D(gl, glc);
		//System.err.printf("testBird loc=%s\n", testBird.pos());
		
		renderBirds(gl, glc);
		shapeList.render3D(gl, glc);

		// render the avatar
		if (viewType == THIRD_PERSON_VIEW) {
			avatar.render3D(gl, glc);
		}
		frameCounter++;
	}
	
	// new method
	private void renderBirds(GL gl, GLCanvas glc2) {
		for (Bird b : birds) {
			b.render3D(gl, glc2);
		}
	}

	// new method
	private void updateBirds() {
		// add new birds every now and then
		if (frameCounter % 300 == 0) {
			addBirds();
			

		}
		boidManager.advance();
	}

	// new method. I didn't finish it.
	private void portalRender(GL gl, GLU glu) {
		// algorithm for drawing view through portals.
		// for each portal in view (or just each portal):
		// -translate the camera (remember the original camera pos) such that it
		// is in the same place in the opposite portal's coord space as it
		// originally was in this portal's coord space
		// -render scene minus this portal to colour buffer
		// -render portal to stencil buffer
		// -translate the camera back
		// render full scene to stencil buffer.
		
		
		// clear depth, color, and stencil buffers
		gl.glClearStencil(0); 
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT
				| GL.GL_STENCIL_BUFFER_BIT);
		

	}

	private void updateAvatar() {
		// Render/move the avatar.
		// do the movement here instead of the event
		// listener so that you can hold a key down
		// to get movement more comfortably. For some reason
		// on my computer (OS X 10.8) holding the key down
		// will only ever fire one event. I've heard it's ok on linux.

		if (keyStates['a'] == KeyState.DOWN) {
			avatar.turn(1);
		}
		if (keyStates['d'] == KeyState.DOWN) {
			avatar.turn(-1);
		}
		if (keyStates['w'] == KeyState.DOWN) {
			avatar.move(1);
		}
		if (keyStates['s'] == KeyState.DOWN) {
			avatar.move(-1);
		}
		avatar.advance(shapeList);
	}

	private void setUpScene(GL gl, GLU glu) {
		// automatically make normals length 1
		gl.glEnable(GL.GL_NORMALIZE);

		// set up lighting
		gl.glEnable(GL.GL_LIGHTING);

		// Use two-sided lighting .
		gl.glLightModeli(GL.GL_LIGHT_MODEL_TWO_SIDE, 1);

		gl.glShadeModel(GL.GL_SMOOTH); // Use Gouraud shading.
		gl.glClearColor(clearColorRed, clearColorGreen, clearColorBlue, 1.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL.GL_COLOR_MATERIAL);
		gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE);
		lightScene(gl);

		// Clear the matrix stack.
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		// If it's a user view, set it from viewpos and viewdir.
		if (viewType == THIRD_PERSON_VIEW) {
			setView(glu, viewpos, viewdir);
		}

		// ... otherwise use information from avatar to do it.
		else {
			setView(glu, avatar.currentPos(), avatar.currentDir());
		}
		
	}

	/** This method handles things if display depth changes */
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	/** For KeyListener */
	public void keyTyped(KeyEvent evt) {

	}

	public void keyReleased(KeyEvent evt) {
		if (evt.getKeyChar() < NUM_KEYS) {
			keyStates[evt.getKeyChar()] = KeyState.UP;
		}
	}

	public void keyPressed(KeyEvent evt) {
		// update the keyboard state
		if (evt.getKeyChar() < NUM_KEYS) {
			keyStates[evt.getKeyChar()] = KeyState.DOWN;
		}

		if (evt.getKeyChar() == 'f') {
			if (viewType == FIRST_PERSON_VIEW) {
				viewType = THIRD_PERSON_VIEW;
			} else {
				viewType = FIRST_PERSON_VIEW;
			}
		}

		if (evt.getKeyChar() == 'v') {
			cycleToNextViewPort();
		}

		if (evt.getKeyChar() == 'p') {
			System.out.println(new ViewPoint(viewpos, viewdir));
		}
		if (evt.getKeyChar() == 'q')
			System.exit(0);

		if (evt.getKeyChar() == 'n') {
			isDay = !isDay;
			if (isDay) {
				clearColorRed = 0.6f;
				clearColorGreen = 0.6f;
				clearColorBlue = 0.9f;
			} else {
				clearColorRed = 0.1f;
				clearColorGreen = 0.1f;
				clearColorBlue = 0.1f;
			}

		}

		if (evt.getKeyChar() == '!') {
			toggleLight(0);
		}
		if (evt.getKeyChar() == '@') {
			toggleLight(1);
		}
		if (evt.getKeyChar() == '#') {
			toggleLight(2);
		}
		if (evt.getKeyChar() == '$') {
			toggleLight(3);
		}
		if (evt.getKeyChar() == 'b') {
			addBirds();
		}

		if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '4') {
			int num = evt.getKeyChar() - '1';

			if (!evt.isShiftDown()) {
				// toggle a door
				List<Door> li = shapeList.getDoors();
				if (li.size() > num) {

					li.get(num).toggleState();
				} else {
					System.err
							.printf("attempted to toggle door %d, list not big enough\n",
									num);
				}
			} else {
				// toggle a light
				List<Light> li = shapeList.getLights();
				if (li.size() > num) {
					li.get(num).toggle();
				} else {
					System.err
							.printf("attempted to toggle light %d, list not big enough\n",
									num);
				}
			}
		}
	}
	// new method
	// adds a few birds to the scene
	private void addBirds() {
		
		// make the birds start behind the current view
		double x, z;
		if (viewType == FIRST_PERSON_VIEW) {
			x = avatar.dir.x;
			z = avatar.dir.z;
		} else {
			x = viewdir.x;
			z = viewdir.z;
		}
		Vector3D startLoc = new Vector3D(x, 0, z)
		.normalize()
		.scale(-1)
		.scale(1000)
		.add(new Vector3D(x, BoidManager.SKY_HEIGHT, z));
		
		for (int i = 0; i < 10; i++) {
			Bird b = new Bird();
			b.setPos(Vector3D.randomNearOrigin().add(startLoc));
			b.addToFlock();
			birds.add(b);
			boidManager.addBoid(b);
		}
		
		// prune birds
		for (int i = birds.size() - 1; i >= 0; i--) {
			if (!boidManager.existsBoid(birds.get(i))) {
				birds.remove(i);
			}
		}
	}

	// new method
	private void cycleToNextViewPort() {
		// don't allow nulls
		if (shapeList == null) {
			return;
		}

		// skip ahead to next ViewPort.
		// count prevents the loop from cycling around the list infinitely
		// found reveals whether a ViewPoint was found or not
		if (currentViewPortIndex < 0) {
			currentViewPortIndex = 0;
		}
		int count = 0;
		boolean found = false;
		while (!found && count < shapeList.size()) {
			currentViewPortIndex++;
			currentViewPortIndex %= shapeList.size();
			count++;
			if (shapeList.get(currentViewPortIndex).getClass() == ViewPoint.class) {
				found = true;
			}
		}

		// change the viewpoint
		if (found) {
			ViewPoint view = (ViewPoint) shapeList.get(currentViewPortIndex);
			viewpos = view.getViewerPos();
			viewdir = view.getViewerDir();

		}
	}

	// Set view, given eye and view direction
	public void setView(GLU glu, Vector3D pos, Vector3D dir) {
		Vector3D look = pos.add(dir);
		glu.gluLookAt(pos.x, pos.y, pos.z, look.x, look.y, look.z, 0, 1, 0);
	}

	/** For MouseListener and MouseMotionListener */

	public void mouseMoved(MouseEvent evt) {

	}

	public void mouseEntered(MouseEvent evt) {

	}

	public void mouseExited(MouseEvent evt) {

	}

	public void mouseClicked(MouseEvent evt) {

	}

	public void mouseReleased(MouseEvent evt) {

	}

	public void mousePressed(MouseEvent evt) {
		prevx = evt.getX();
		prevy = evt.getY();
	}

	// Do view changing.
	public void mouseDragged(MouseEvent evt) {
		if (viewType == THIRD_PERSON_VIEW) {
			double xdiff = ((double) evt.getX() - prevx);
			double ydiff = ((double) evt.getY() - prevy);

			if (evt.isMetaDown()) {
				// do dollying
				viewpos = viewpos.subtract(viewdir.normalize().scale(ydiff));
			}

			else {
				// do rotation
				viewdir = viewdir.rotatey(-xdiff / ROTATE_FACTOR);
				viewdir = viewdir.rotateUpDown(ydiff / ROTATE_FACTOR);

				/*
				 * // awesome hack for up/down: modify y component, normalise //
				 * went with a better idea, see Vector3D.rotateUpDown viewdir.y
				 * -= ydiff/ROTATE_FACTOR; viewdir = viewdir.normalize();
				 */
			}
			prevx = evt.getX();
			prevy = evt.getY();
		}

	}

}
