import java.util.ArrayList;
import java.util.List;

/**
 * A class that simulates a flock of bird-like objects using the
 * boids (Reynolds, 1987) algorithm.
 * 
 * The pseudocode from:
 * http://www.vergenet.net/~conrad/boids/pseudocode.html
 * was the primary source of inspiration and information.
 * 
 * The wikipedia article http://en.wikipedia.org/wiki/Boids
 * helped a lot also.
 * 
 * The Processing article was also used for inspiration, although
 * no code was directly copied http://processing.org/learning/topics/flocking.html
 * 
 * @author Luke Cameron
 * 
 */

public class BoidManager {
	
	final double MIN_DISTANCE = 30;
	final double VEL_MATCH_SCALE = 0.12;
	public static final double SKY_HEIGHT = 300;
	public static final double VEL_MATCH_CUTOFF = 5;
	public static final double CIRCLE_DISTANCE = 1000;
	public static final double VISION = 150;
	
	// limit number of boids for performance reasons
	final int HARD_LIMIT = 500;
	
	private List<IBoid> boids;
	
	// these attract in the xz plane, because
	// birds like to fly in the sky
	private List<IBoidObject> attractors;
	
	public BoidManager() {
		boids = new ArrayList<IBoid>();
		attractors = new ArrayList<IBoidObject>();
	}
	
	public void addBoid(IBoid b) {
		// delete a boid
		if (boids.size() >= HARD_LIMIT) {
			boids.remove(0);
		}
		boids.add(b);
		System.err.printf("boid added. now simulating %d boids\n", boids.size());
	}
	
	public boolean existsBoid(IBoid b) {
		return boids.contains(b);
	}
	
	public void addAttractor(IBoidObject o) {
		attractors.add(o);
		System.err.printf("attractor added. now simulating %d attractors\n", attractors.size());
	}
	
	public IBoidObject remAttractor(IBoidObject o) {
		return attractors.remove(attractors.indexOf(o));
	}
	
	public IBoid remBoid(IBoid b) {
		return boids.remove(boids.indexOf(b));
	}
	
	public void advance() {
		// the main boids algorithm
		
		for (IBoid b : boids) {
			// main boids rules
			Vector3D coh = cohesion(b).scale(0.0001);
			Vector3D sep = separation(b).scale(0.001);
			Vector3D ali = alignment(b).scale(0.001);
			
			// other rules
			Vector3D sky = sky(b).scale(0.01);
			Vector3D atr = atr(b).scale(0.009);
			
			b.setAcc(b.vel().add(coh).add(sep).add(ali).add(sky).add(atr));
			//System.err.printf("boid sim step\n\tcoh:%s\n\tsep:%s\n\tali:%s\n", coh, sep, ali);
		}
		for (IBoid b : boids) {
			b.update();
		}
	}
	
	private Vector3D atr(IBoid b) {
		Vector3D attract = new Vector3D(0,0,0);
		for (IBoidObject o : attractors) {
			Vector3D distance = o.pos().subtract(b.pos()).remY();
			if (distance.length() > CIRCLE_DISTANCE) {
				attract = attract.add(distance.scale(0.01));
			}
			else {
				// if getting close, use an orthogonal vector
				attract = attract.add(distance.scale(0.01).cross(new Vector3D (0,1,0)));
			}
		}
		
		return attract;
	}

	private Vector3D sky(IBoid b) {
		if (b.pos().y > SKY_HEIGHT + 50 || b.pos().y < SKY_HEIGHT - 50) {
			return new Vector3D(0, (SKY_HEIGHT-b.pos().y)/100, 0);
		}
		return new Vector3D(0,0,0);
	}

	private Vector3D cohesion(IBoid b) {
		// the rule from vergenet is "Boids try to fly towards the centre of mass of neighbouring boids"
		
		Vector3D center = null;
		double count = 0;
		for (IBoid cur : boids) {
			if (b != cur && shouldInclude(cur, b)) {
				if (center == null) {
					center = cur.pos();
				} else {
					center = center.add(cur.pos());
				}
				count++;
			}
		}
		
		if (count != 0) {
			return center.scale(1/count).subtract(b.pos());
		
		} else {
			return b.pos();
		}
	}
	
	private Vector3D separation(IBoid b) {
		// the rule from vergenet is "Boids try to keep a small distance away from other objects"
		
		Vector3D offset = new Vector3D(0,0,0);
		
		for (IBoid cur : boids) {
			if (cur != b && shouldInclude(b, cur)) {
				if (cur.pos().subtract(b.pos()).length() < MIN_DISTANCE) {
					offset = offset.subtract(cur.pos().subtract(b.pos()));
				}
			}
		}
		return offset;
	}

	private Vector3D alignment(IBoid b) {
		// the rule from vergenet is "Boids try to match velocity with near boids"
		
		Vector3D avgVel = new Vector3D(0,0,0);
		double count = 0;
		
		for (IBoid cur : boids) {
			if (b != cur && shouldInclude(b, cur)) {
				if (cur.vel().length() < VEL_MATCH_CUTOFF) {
					avgVel = avgVel.add(cur.vel());
					count++;
				}
			}
		}
		if (count != 0) {
			avgVel = avgVel.scale(1/count);
		}
		return avgVel.subtract(b.vel()).scale(VEL_MATCH_SCALE);
	}
	
	private boolean shouldInclude(IBoid b1, IBoid b2) {
		double distance = b1.pos().subtract(b2.pos()).length();
		return distance < VISION;
	}




}
