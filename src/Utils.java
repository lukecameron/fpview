import javax.media.opengl.GL;

import com.sun.opengl.util.texture.Texture;

/**
 * a few drawing functions that I wanted to share between classes.
 * 
 * 
 * @author Luke Cameron
 * 
 */
public class Utils {

	public static final int TEXTURE_SQUISH = 1;
	public static final int TEXTURE_COPY = 2;

	/**
	 * Draw a rectangle.
	 * 
	 * @param gl
	 *            - the context to draw to
	 * 
	 * @param points
	 *            - points given in cyclic order for RH normal calculation. -
	 *            assumes they are coplanar
	 */
	public static void drawRectangle(GL gl, Vector3D[] points) {
		drawRectangleFlexible(gl, points, false, null, 0, 0);
	}

	/**
	 * Draw a rectangle textured using the 'squish' scheme (i.e. for doors and
	 * portals)
	 * 
	 * @param gl
	 *            - the context to draw to
	 * 
	 * @param points
	 *            - points given in cyclic order for RH normal calculation.
	 * 
	 *            - assumes they are coplanar
	 * 
	 *            - for texturing to be correct, the order is assumed to be
	 *            (with normal facing viewer): bottom-left, top-left, top-right,
	 *            bottom-right
	 * 
	 */
	public static void drawRectangleTexturedSquish(GL gl, Vector3D[] points,
			Texture tex, double scaleFactor) {
		drawRectangleFlexible(gl, points, true, tex, TEXTURE_SQUISH,
				scaleFactor);
	}

	/**
	 * Draw a rectangle textured using the 'copy 100' scheme (i.e. for walls and
	 * windows)
	 * 
	 * @param gl
	 *            - the context to draw to
	 * 
	 * @param points
	 *            - points given in cyclic order for RH normal calculation.
	 * 
	 *            - assumes they are coplanar
	 * 
	 *            - for texturing to be correct, the order is assumed to be
	 *            (with normal facing viewer): bottom-left, top-left, top-right,
	 *            bottom-right
	 */
	public static void drawRectangleTexturedCopy(GL gl, Vector3D[] points,
			Texture tex, double scaleFactor) {
		drawRectangleFlexible(gl, points, true, tex, TEXTURE_COPY, scaleFactor);
	}

	// the flexible version of the above methods that actually does the work.
	// Not intended to be used by other classes.
	private static void drawRectangleFlexible(GL gl, Vector3D[] points,
			boolean texture, Texture tex, int textureMode, double scaleFactor) {
		// ensure array has correct number of points
		if (points.length != 4) {
			return;
		}

		// calculate normal by taking cross of first two edges
		Vector3D e1 = points[1].subtract(points[0]);
		Vector3D e2 = points[2].subtract(points[1]);
		Vector3D nor = e1.cross(e2).normalize();

		// more values to calculate
		double height = e1.length();
		double width = e2.length();
		Point2D origin = new Point2D(points[0].x, points[0].y);

		Point2D[] texSquishCoords = { new Point2D(0, 0),
				new Point2D(0, scaleFactor),
				new Point2D(scaleFactor, scaleFactor),
				new Point2D(scaleFactor, 0) };
		Point2D[] texCopyCoords = {
				new Point2D(0, 0).add(origin),
				new Point2D(0, height * scaleFactor / 100).add(origin),
				new Point2D(width * scaleFactor / 100, height * scaleFactor
						/ 100).add(origin),
				new Point2D(width * scaleFactor / 100, 0).add(origin) };

		// set up texture (with fancy filtering)
		if (texture && tex != null) {
			gl.glEnable(GL.GL_TEXTURE_2D);

			tex.enable();
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
					GL.GL_REPEAT);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
					GL.GL_REPEAT);
			gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE,
					GL.GL_MODULATE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
					GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
					GL.GL_LINEAR);
			tex.bind();
		}

		// draw the rectangle
		gl.glBegin(GL.GL_QUADS);
		gl.glNormal3d(nor.x, nor.y, nor.z);
		int count = 0;
		for (Vector3D p : points) {
			if (texture && textureMode == TEXTURE_COPY) {
				gl.glTexCoord2d(texCopyCoords[count].x, texCopyCoords[count].y);
			} else if (texture && textureMode == TEXTURE_SQUISH) {
				gl.glTexCoord2d(texSquishCoords[count].x,
						texSquishCoords[count].y);
			}
			gl.glVertex3d(p.x, p.y, p.z);
			count++;
		}
		gl.glEnd();
	}

}
