== Running the program ==
java FPView file.fpe [--edit]

== Notes to the marker ==
The edit flag opens FPed in a second window which allows modification of the shapes in the scene with live updates in the FPview window. The flag was added for testing purposes and the original idea came from Damon Stacey. While it can be considered a bonus feature, the idea is not my own.

For files that are changed, I've included a comment at the top explaining what about the file has changed. I've also included an @author parameter specifying "Tim Lambert". In these cases I really mean "the original author(s) of the file".

I've also tried to add the comment "// new method" above any new methods, and "// changed method" above any changed methods.

For unchanged files I've tried to leave them alone so that they don't show up in a diff. Occasionally they might be auto-indented.

== bonus features ==
As mentioned above there is a live editing mode.

Lights fade in and out when switched on and off.

Occasionally, architecture work can be tedious/stressful. To help combat this, every few seconds birds are added to the scene. They will flock together in groups and eventually circle the avatar at a respectable distance of 1000 units.

If more birds are desired, they can be added in groups of ten with the 'b' key.