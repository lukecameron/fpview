import java.awt.Color;
import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLDrawable;

import com.sun.opengl.util.GLUT;

/**
 * A class that represents a bird and implements a boid.
 * 
 * 
 * @author Luke Cameron
 * 
 */

public class Bird implements IBoid {
	Vector3D pos;
	Vector3D vel;
	Vector3D acc;
	
	IForcer grav;
	boolean inFlock;
	
	final double MAX_VEL = 10;
	
	double flapPeriod = 5; // 2 flaps per second
	double flapIncrement = 40/flapPeriod; // in degrees. a guess.
	double currentFlapAngle = 0;
	double maxFlap = 20;
	double minFlap = -20;
	boolean flappingUp = true;
	
	
	
	public Bird() {
		pos = new Vector3D(0,0,0);
		vel = new Vector3D(0,0,0);
		acc = new Vector3D(0,0,0);
		grav = null;
		inFlock = false;
	}
	
	public void render3D(GL gl, GLDrawable glc) {
		float[] diffuseColour = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE,
				FloatBuffer.wrap(diffuseColour));
		gl.glDisable(GL.GL_TEXTURE_2D);
		
		gl.glPushMatrix();
		gl.glTranslated(pos.x, pos.y, pos.z);
		if (vel.length() != 0) {
			gl.glRotated(Math.toDegrees(dir().azimuth()), 0, 1, 0);
			gl.glRotated(Math.toDegrees(dir().elevation()), 1, 0, 0);
		}
		
		// draw body
		gl.glPushMatrix();
		gl.glScaled(1, 3, 0.5);
		GLUT glut = new GLUT();
		glut.glutSolidCube(10);
		gl.glPopMatrix();
		
		// draw left wing
		gl.glPushMatrix();
		gl.glRotated(-currentFlapAngle, 0, 1, 0);
		gl.glTranslated(-10, 0, 0);
		
		gl.glScaled(3, 3, 0.5);
		
		glut.glutSolidCube(5);
		gl.glPopMatrix();
		
		// draw right wing
		gl.glPushMatrix();
		gl.glRotated(currentFlapAngle, 0, 1, 0);
		gl.glTranslated(10, 0, 0);
		
		gl.glScaled(3, 3, 0.5);
		
		glut.glutSolidCube(5);
		gl.glPopMatrix();
		
		
		gl.glPopMatrix();
	}

	@Override
	public void update() {
		// a nice, simple euler integrator
		if (!inFlock && grav != null) {
			acc.add(grav.acc());
		}
		
		// integrate velocity
		vel = vel.add(acc);
		
		// limit velocity
		if (vel.length() > MAX_VEL) {
			vel = vel.normalize().scale(MAX_VEL);
		}
		
		// integrate position
		pos = pos.add(vel);
		//System.err.printf("bird update. acc=%s, vel=%s\n", acc, vel);
		
		// flapping
		if (flappingUp && currentFlapAngle < maxFlap) {
			currentFlapAngle += flapIncrement;
		}
		
		else if (!flappingUp && currentFlapAngle > minFlap) {
			currentFlapAngle -= flapIncrement;
		}
		
		else if (currentFlapAngle > maxFlap) {
			currentFlapAngle -= flapIncrement;
			flappingUp = false;
		}
		
		else if (currentFlapAngle < minFlap) {
			currentFlapAngle += flapIncrement;
			flappingUp = true;
		}
	}
	
	@Override
	public Vector3D pos() {
		return pos;
	}

	@Override
	public Vector3D vel() {
		return vel;
	}

	@Override
	public Vector3D dir() {
		return vel.normalize();
	}

	@Override
	public void setPos(Vector3D pos) {
		this.pos = pos;
	}

	@Override
	public void setVel(Vector3D vel) {
		this.vel = vel;
	}

	@Override
	public void setAcc(Vector3D acc) {
		this.acc = acc;
	}

	@Override
	public void setGravity(IForcer f) {
		grav = f;
	}

	@Override
	public void addToFlock() {
		inFlock = true;
	}

	@Override
	public void removeFromFlock() {
		inFlock = false;
	}


}
