/**
 * A very simple key state enum
 * 
 * @author Luke Cameron
 */

public enum KeyState {
	DOWN, UP
}
