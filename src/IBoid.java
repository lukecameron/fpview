/**
 * An interface that represents a boid for the
 * BoidManager class.
 * 
 * 
 * @author Luke Cameron
 * 
 */

public interface IBoid {
	public Vector3D pos();
	public Vector3D vel();
	public Vector3D dir();
	
	public void setPos(Vector3D pos);
	public void setVel(Vector3D vel);
	public void setAcc(Vector3D acc);
	public void setGravity(IForcer f);
	public void addToFlock(); // causes the boid to not update its own vel and dir.
	public void removeFromFlock(); // causes the boid to act independantly
	public void update(); // integrates pos according to vel and vel according to acc and its own devices (e.g. gravity).
}
